#!/usr/bin/python
import ConfigParser,os
config = ConfigParser.RawConfigParser()

config.add_section('Global')


config.set('Global', 'comemaBasedir', os.path.join(os.path.expanduser('~'),
              '.comema'))
config.set('Global','comemaQueueDir','/home/cfarrell/comema/queue/')
config.set('Global','comemaProcessedDir','/home/cfarrell/comema/processed/')
config.set('Global','raceConditionTimeout',60) # minutes
with open('comema.cfg', 'wb') as configfile:
    config.write(configfile)             
