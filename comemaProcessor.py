import ConfigParser,hashlib,os,sys
try:
  from pdfrw import PdfReader, PdfWriter, IndirectPdfDict
  from pdfrw.errors import PdfParseError
except ImportError:
  sys.stderr.write('\n')
  sys.exit(1)

config = ConfigParser.SafeConfigParser()
config.read('comema.cfg')

class comemaProcessor:
  def __init__(self,inputfile,**kw):
    '''Assuming the caller has already checked validity of inputfile
    and has checked the **kw values appropriately'''
    self.orig = inputfile
    self.orig_filename = os.path.basename(self.orig)
    self.newPDF = PdfWriter()
    if kw.has_key('parties'):
      self.parties = kw['parties']
    if kw.has_key('desc'):
      self.desc = kw['desc']
    if kw.has_key('tags'):
      self.tags = kw['tags']
    if kw.has_key('effDate'):
      self.effDate = kw['effDate']
    if kw.has_key('termDate'):
      self.termDate = kw['termDate']
    if kw.has_key('notice'):
      self.notice = kw['notice']
    if kw.has_key('autoRenew'):
      self.autoRenew = kw['autoRenew']
    if kw.has_key('relatedTo'):
      self.relatedTo = kw['relatedTo']
    if kw.has_key('signatories'):
      self.signatories = kw['signatories']
    if kw.has_key('status'):
      self.status = kw['status']      

  def createUniqId(self):
    '''Using the sha256 of the original document, create a uniq id
    - this way we can check if an apparently new document has already
    been processed'''
    with open(self.orig) as f:
        m = hashlib.sha256()
        m.update(f.read())
        sha = m.hexdigest()
        #res = base64.b64encode(sha)
    return sha
    
  def createQRCover(self):
    '''If requested, encode the metadata in a qr code and
    create a cover page for the document - alternatively try
    to insert a small qr code overlay'''
    pass
      
  def createOutputFile(self):
    uniqId = self.createUniqId()
    dropext = self.orig_filename.replace('.pdf','')
    sanitized = ''.join(x for x in dropext if x.isalnum())
    outputfile = os.path.join(config.get('Global','comemaProcessedDir'),
                  '%s.pdf'%sanitized)
    try:
      parsedPDF = PdfReader(self.orig)
    except PdfParseError:
      raise
    else:
      """ use # to separate parties, signatories etc
          as exiftool uses a : to print results and the custom egrep we 
          use in recoll's rclpdf is stupid and if we use : to separate parties
          etc, it will only return the last party"""
      self.newPDF.addpages(parsedPDF.pages)
      self.newPDF.trailer.Info = IndirectPdfDict(
        effDate = self.effDate,
        termDate = self.termDate,
        autoRenew = self.autoRenew,
        notice = self.notice,
        parties = '#'.join(self.parties),
        tags = '#'.join(self.tags),
        relatedTo = self.relatedTo,
        signatories = '#'.join(self.signatories),
        uniqueId = uniqId,
        status = self.status,
        desc = self.desc
      )
    try:
      self.newPDF.write(outputfile)
    except:
      raise
    else:
      return outputfile
      
  def tryAgain(self):
    uniqId = self.createUniqId()
    dropext = self.orig_filename.replace('.pdf','')
    sanitized = ''.join(x for x in dropext if x.isalnum())
    outputfile = os.path.join(config.get('Global','comemaProcessedDir'),
                  '%s.pdf'%sanitized)
    from pyPdf import PdfFileWriter, PdfFileReader
    from pyPdf.generic import NameObject, createStringObject
    
    output = PdfFileWriter()
    input1 = PdfFileReader(file(self.orig, "rb"))
    infoDict = output._info.getObject()
    infoDict.update({
    NameObject('/EffDate'): createStringObject(u'%s'%self.effDate.strftime('%Y-%m-%d')),
    NameObject('/TermDate'): createStringObject(u'%s'%self.termDate),
    NameObject('/Description'): createStringObject(u'%s'%self.desc),
    NameObject('/Status'): createStringObject(u'%s'%self.status),
    NameObject('/ComemaId'): createStringObject(u'%s'%self.createUniqId()),
    NameObject('/Tags'): createStringObject(u'%s'%'#'.join(self.tags)),
    NameObject('/Parties'): createStringObject(u'%s'%'#'.join(self.parties)),
    NameObject('/RelatedTo'): createStringObject(u'%s'%self.relatedTo),
    NameObject('/Signatories'): createStringObject(u'%s'%'#'.join(self.signatories)),
    NameObject('/Notice'): createStringObject(u'%s'%self.notice),
    NameObject('/AutoRenew'): createStringObject(u'%s'%self.autoRenew),
    })
    for page in range(input1.getNumPages()):
        output.addPage(input1.getPage(page))
    outputStream = file(outputfile, 'wb')
    output.write(outputStream)
    outputStream.close()
