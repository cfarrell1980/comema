#!/usr/bin/env python
from recoll import recoll
import os,base64,subprocess,ConfigParser
config = ConfigParser.SafeConfigParser()
config.read('comema.cfg')
proc_dir = config.get('Global','comemaProcessedDir')

from datetime import datetime

class comemaAPI:
  def __init__(self):
    pass
    
  def getMetaFromFile(self,fn):
    '''Use exiftool to get metadata from file'''
    fp = os.path.join(proc_dir,fn)
    cmd = subprocess.Popen(['exiftool',fp], stdout=subprocess.PIPE)
    output = cmd.stdout.read()
    as_lines = output.split('\n')
    meta = {}
    for l in as_lines:
      if l.lower().startswith('comema'):
        meta['comemaid'] = l.split(':')[1].replace(' ','')
      elif l.lower().startswith('effdate'):
        meta['effdate'] = l.split(':')[1].replace(' ','')
      elif l.lower().startswith('termdate'):
        meta['termdate'] = l.split(':')[1].replace(' ','')
      elif l.lower().startswith('parties'):
        meta['parties'] = l.split(':')[1].replace(' ','')
      elif l.lower().startswith('tags'):
        meta['tags'] = l.split(':')[1].replace(' ','')
      elif l.lower().startswith('desc'):
        meta['desc'] = l.split(':')[1].replace(' ','')
      elif l.lower().startswith('notice'):
        meta['notice'] = l.split(':')[1].replace(' ','')
      elif l.lower().startswith('autorenew'):
        meta['autorenew'] = l.split(':')[1].replace(' ','')
      elif l.lower().startswith('signatories'):
        meta['signatories'] = l.split(':')[1].replace(' ','')
      elif l.lower().startswith('status'):
        meta['status'] = l.split(':')[1].replace(' ','')
      elif l.lower().startswith('related'):
        meta['related'] = l.split(':')[1].replace(' ','')
      else:
        continue
    return meta
    
  def doQuery(self,q,for_json=False):
    db = recoll.connect()
    db.setAbstractParams(maxchars=1200, contextwords=4)
    query = db.query()
    nres = query.execute(q)
    rlist = []
    for i in range(nres):
      doc = query.fetchone()
      try:
        effdate = datetime.strptime(getattr(doc,'effdate'),'%Y-%m-%d')
      except ValueError:
        effdate = getattr(doc,'effdate')
      try:
        termdate = datetime.strptime(getattr(doc,'termdate'),'%Y-%m-%d')
      except ValueError: # could be 'until terminated
        termdate = getattr(doc,'termdate')
      parties_raw = getattr(doc,'parties')
      if '#' in parties_raw: # more than one party
        parties = parties_raw.split('#')
      else: parties = parties_raw
      signatories_raw = getattr(doc,'signatories')
      if '#' in signatories_raw: # more than one party
        signatories = signatories_raw.split('#')
      else: signatories = signatories_raw
      tags_raw = getattr(doc,'tags')
      if '#' in tags_raw: # more than one party
        tags = tags_raw.split('#')
      else: tags = tags_raw
      uri = doc.getbinurl()
      if for_json:
        # returning via web, we only need the filename - no need to compromise
        # security by sending the abspath
        uri = os.path.basename(uri)
        if isinstance(effdate,datetime):
          effdate = effdate.strftime('%d %b %Y') # javascript has no strftime
        if isinstance(termdate,datetime):
          termdate = termdate.strftime('%d %b %Y')
      description = getattr(doc,'description') # TODO check out wtf is going on here
      if termdate == 'until terminated': termdate = 'terminated' # until until terminated in web ui looks stupid
      if description == '' or description == None:
        # notify admin
        description = 'Unfortunately this agreement does not have a description. The administrator has been notified and will hopefully add a description soon'
      rdict = { 'idx':query.rownumber,
                'uri':uri,
                'b64uri':base64.b64encode(uri),
                'effdate':effdate,
                'termdate':termdate,
                'description':description,
                'size':getattr(doc,'size'),
                'signatories':signatories,
                'status':getattr(doc,'status'),
                'notice':getattr(doc,'notice'),
                'autorenew':getattr(doc,'autorenew'),
                'parties':parties,
                'tags':tags,
                'relatedto':getattr(doc,'relatedto'),
                'comemaid':getattr(doc,'comemaid')}
      rlist.append(rdict)
    return rlist
    
if __name__ == '__main__':
  import sys
  q1 = sys.argv[1]
  comema = comemaAPI()
  x = comema.doQuery(q1)
  print x
