#!/usr/bin/python
"""
This script is called by cron. It looks at all the files in the queue
directory and does:
(1) check the ctime - if very recent, don't touch in case a race condition is
created with e.g. a web user uploading a new document. By 'very recent' meant is
whether raceConditionTimeout minutes have elapsed since ctime.
(2) check if there is a document in the processed directory with a comemaid
matching the sha256 sum of the one in the queue directory. If so, delete the
document in the queue directory
(3) if there is no document in processed and the ctime of the document in the
queue directory is old, alert the admin
"""
import ConfigParser,hashlib,os,subprocess
from datetime import datetime, timedelta
config = ConfigParser.SafeConfigParser()
config.read('comema.cfg')

queue_dir = config.get('Global','comemaQueueDir')
proc_dir = config.get('Global','comemaProcessedDir')
rcTimeout = int(config.get('Global','raceConditionTimeout'))

l_queue_dir = [os.path.join(queue_dir,x) for x in os.listdir(queue_dir)]
l_proc_dir = [os.path.join(proc_dir,x) for x in os.listdir(proc_dir)]

queue_coll = []
queue_dict = {}
collector_dict = {}
for fp in l_queue_dir:
  with open(fp) as f:
    m = hashlib.sha256()
    m.update(f.read())
    sha = m.hexdigest()
    queue_dict[sha] = fp

for fp in l_proc_dir:
  cmd = subprocess.Popen(['exiftool',fp], stdout=subprocess.PIPE)
  output = cmd.stdout.read()
  as_lines = output.split('\n')
  for l in as_lines:
    if l.lower().startswith('comema'): # comema id is the sha256 sum
      sha256 = l.split(':')[1].replace(' ','')
      collector_dict[sha256] = fp

for queue_sum in queue_dict.keys():
  if collector_dict.has_key(queue_sum): # already processed?
    try:
      #print "attempting to rm %s as there is a corresponding processed entry"%queue_dict[queue_sum]
      os.unlink(queue_dict[queue_sum])
    except Exception,e:
      pass
      #print "failed to rm %s: %s"%(queue_dict[queue_sum],str(e))
  else: # queue entry but no processed entry
    ctime = datetime.fromtimestamp(os.stat(queue_dict[queue_sum])[-1])
    delta = timedelta(minutes=rcTimeout)
    now = datetime.now()
    if ctime < now-delta:
      try:
        #print "attempting to rm %s as it is older that allowed threshold"%queue_dict[queue_sum]
        os.unlink(queue_dict[queue_sum])
      except Exception,e:
        pass
        #print "failed to rm %s: %s"%(queue_dict[queue_sum],str(e))
