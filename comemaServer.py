#!/usr/bin/env python
import web,sys,os,logging,json,ConfigParser,base64,hashlib,subprocess
from datetime import datetime
from comemaAPI import comemaAPI
try:
  from comemaProcessor import comemaProcessor
except ImportError,e: # if comemaProcessor is missing an import
  raise e
web.template.Template.globals['render'] = web.template.render
render = web.template.render('templates/', base='layout')
urls =  (
          '/', 'start',
          '/start.html','start',
          '/find.html','find',
          '/add.html','add',
          '/help.html','help',
          '/ocr.html','ocr',
          '/faq.html','faq',
          '/getfile/(.+)','getFile',
          '/xhr-json-comema-query','xhrJsonComemaQuery',
          '/xhr-process-add','xhrProcessAdd',
          '/xhr-process-pdf','xhrProcessPdf',
          '/xhr-getby-uri','xhrGetByUri',
        )
app = web.application(urls, globals())
c_api = comemaAPI()
  


class start:
  def GET(self):
    return render.start()

class find:
  def GET(self):
    return render.find()

class add:
  def GET(self):
    return render.add()
    
class help:
  def GET(self):
    return render.help()

class ocr:
  def GET(self):
    return render.ocr()

  def POST(self):
    print "post called"
    store = PDF2Disk(web.input())
    return render.ocr()
    
class faq:
  def GET(self):
    return render.faq()

class xhrGetByUri:
  def POST(self):
    uri = web.input().uri
    web.header('Content-Type','application/json')
    try:
      q = c_api.getMetaFromFile(uri)
    except Exception,e:
      print e
      
    return json.dumps({'meta':q})
    
class xhrJsonComemaQuery:
  def POST(self):
    web.header('Content-Type', 'application/json')
    cqt = web.input().comemaquery
    q = c_api.doQuery(cqt,for_json=True)
    return json.dumps(q)
    
class getFile:
  def GET(self,uri):
    try:
      f = open(os.path.join('/home/cfarrell/comema/processed',uri), 'r')
      mem = f.read()
      f.close()
      web.header('Content-Type', 'application/pdf')
      return mem
    except:
      return 'fuckit'

class xhrProcessAdd:
  def POST(self):
    data = web.input()
    effdate = data.effdate
    termdate = data.termdate
    sha = data.sha
    description = data.description
    parties = data.parties
    tags = data.tags
    signatories = data.signatories
    autorenewal = data.autorenewal
    notice = data.notice
    fail = False
    r = { 'effdate':{'msg':'','val':effdate},
          'termdate':{'msg':'','val':termdate},
          'sha':{'msg':'','val':sha},
          'description':{'msg':'','val':description},
          'parties':{'msg':'','val':parties},
          'tags':{'msg':'','val':tags},
          'signatories':{'msg':'','val':signatories},
          'autorenewal':{'msg':'','val':autorenewal},
          'notice':{'msg':'','val':notice}
        }
    try:
      effdate = datetime.strptime(effdate,'%Y-%m-%d')
    except Exception,e:
      print e
      r['effdate']['msg'] = 'Effective date must be in YYYY-MM-DD format'
      fail = True
    try:
      termdate = datetime.strptime(termdate,'%Y-%m-%d')
    except Exception,e:
      if termdate != 'until terminated':
        if termdate[-1] != 'y' and termdate[-1] != 'm':
          r['effdate']['msg'] = 'Termination date must be either in YYYY-MM-DD format or expressed relative to Effective Date (e.g. 2y, 6m)'
          fail = True
    if not sha or sha == '':
      r['sha']['msg'] = 'Could not associate the metadata with a pdf document'
      fail = True
    else:
      # loop through files in queue to see if we can find the right one
      tmpdir = '/home/cfarrell/comema/queue/'
      keeper = None
      for f in os.listdir(tmpdir):
        fp = os.path.join(tmpdir,f)
        fd = open(fp,'r')
        m = hashlib.sha256()
        m.update(fd.read())
        shaT = m.hexdigest()
        fd.close()
        if shaT == sha:
          keeper = fp
      if not keeper:
        r['sha']['msg'] = 'Could not associate the metadata with a pdf document'
        fail = True
        
    status = 'unknown'
    relatedto = None
    
    tags = tags.split('#')
    parties = parties.split('#')
    signatories = signatories.split('#')
    if not fail:
      try:
        c_proc = comemaProcessor(keeper,effDate=effdate,termDate=termdate,autoRenew=autorenewal,
          notice=notice,tags=tags,parties=parties,relatedTo=relatedto,
          signatories=signatories,status=status,desc=description)
        c_proc.createOutputFile()
      except Exception,e:
        print e
        return json.dumps({'status':1})
      else:
        #p = subprocess.Popen(['/usr/bin/recollindex'])
        return json.dumps({'status':0})
    else:
      return json.dumps(r)

def FireAndForget(pdf_fname):
  without_ext = pdf_fname[:-4]# is this stable enough to strip '.pdf' irrespective of case?
  outname = "%s.ocr.pdf"%without_ext
  c = "docker run -v '/home/cfarrell/comema/queue:/home/docker/' paulstaab/ocrmypdf OCRmyPDF  -o 300 %s %s"%(pdf_fname,outname)
  os.system("%s &"%c)
  return
    
class xhrProcessPdf:
  def POST(self):
    print "Yeehaw!"
    web.header('Content-Type', 'application/json')
    rawdata = web.input().data
    fname = web.input().filename
    splitdata = rawdata.split(',')
    b64data = splitdata[1]
    decoded = base64.b64decode(b64data)
    mtype = splitdata[0][5:-8]
    queuedir = '/home/cfarrell/comema/queue/'
    tmpname = os.path.join(queuedir,fname)
    try:
      fd = open(tmpname,'wb')
      fd.write(decoded)
      fd.close()
    except Exception,e:
      print e
      return json.dumps({'status':1})
    else:
      fd = open(tmpname,'r')
      m = hashlib.sha256()
      m.update(fd.read())
      sha = m.hexdigest()
      fd.close()
      fsize = os.path.getsize(tmpname)
      FireAndForget(fname)
    return json.dumps({'status':0,'fname':fname,'fsize':fsize,'sha256':sha})


if __name__ == "__main__":
    app.run()
