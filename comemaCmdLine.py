import ConfigParser,sys,os,argparse
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta
from comemaProcessor import comemaProcessor
try:
  from pdfrw import PdfReader, PdfWriter, IndirectPdfDict
  from pdfrw.errors import PdfParseError
except ImportError:
  sys.stderr.write('You no got pdfrw\n')
  sys.exit(1)
config = ConfigParser.SafeConfigParser()
config.read('comema.cfg')

parser = argparse.ArgumentParser(description='Do the heavy lifting...')
parser.add_argument('--eff', dest='effDate',
      help='effective date of this agreement (format YYYY-MM-DD)')
parser.add_argument('--term', dest='termDate',
      help='termination date of this agreement (format YYYY-MM-DD or relatives)')
parser.add_argument('--autorenew', dest='autoRenew',
      help='if agreement autorenews enter the autorenew period in months or years (e.g. 1y, 18m)')
parser.add_argument('infile', nargs="+", 
      help='one single pdf file (assumed to be the fully executed agreement) is accepted')
parser.add_argument('--tag', dest='tags',action='append',
      help='tags you want to associate with this agreement')
parser.add_argument('--party', dest='parties',action='append',
      help='parties you want to associate with this agreement')
parser.add_argument('--sig', dest='signatories',action='append',
      help='signatories to the agreement')
parser.add_argument('--related-to', dest='relatedTo',action='append',
      help='unique id of an agreement you want to associate this one with')
parser.add_argument('--notice', dest='notice',
      help='how many days notice before autorenew/termination occurs')
parser.add_argument('--status', dest='status',
      help='status can be either active, expired, terminated, amended or unknown. Default is to guess based on effDate and termDate')
parser.add_argument('--desc', dest='desc',
      help='accurate description of the agreement (particularly useful for searching later)')
args = parser.parse_args()

if not len(args.infile)==1:
  parser.error('You have to provide one pdf file - assumed to be the fully executed agreement')
else:
  # check if file is pdf
  if not os.path.isfile(args.infile[0]):
    parser.error('%s is not a file'%args.infile[0])
  #try:
  #  parsedPDF = PdfReader(args.infile[0])
  #except PdfParseError:
  #  parser.error("%s is not a fuggin PDF"%args.infile[0])
  else:
    infile = os.path.abspath(args.infile[0])
    # parsedPDF is still open - we can use it
if not args.effDate:
  parser.error('You have to provide an effective date')
try:
  effDate = datetime.strptime(args.effDate,'%Y-%m-%d')
except ValueError:
  parser.error('The effective date must be in YYYY-MM-DD format (and must be a valid date)')
if not args.termDate or args.termDate == 'until terminated':
  termDate = 'until terminated'
else:
  try:
    termDate = datetime.strptime(args.termDate,'%Y-%m-%d')
  except ValueError:
    # maybe termDate was provided as relative to effDate (months or years)
    # see if the last char is 'y' or 'm'
    if len(args.termDate) > 1:
      if args.termDate[-1] == 'y':
        try:
          termrelval = int(args.termDate[:-1])
        except ValueError:
          parser.error('A relative termination date must be expressed as integer followed immediately by either m for month or y for year')
        reldelta = relativedelta(years=termrelval)
      elif args.termDate[-1] == 'm':
        try:
          termrelval = int(args.termDate[:-1])
        except ValueError:
          parser.error('A relative termination date must be expressed as integer followed immediately by either m for month or y for year')
        reldelta = relativedelta(months=termrelval)
      
      else:
        parser.error('Only m (month) and y (year) are supported as relative time deltas for expressing the termination date')
      termDate = effDate + reldelta
if termDate != "until terminated":
  try:
    termDate = termDate.strftime("%Y-%m-%d")
  except:# maybe termDate was a string?
    parser.error('termDate is neither a string nor datetime????')

if args.autoRenew:
  if len(args.autoRenew) > 1:
    if args.autoRenew[-1] == 'y':
      try:
        autoRenew = "%d year periods"%int(args.autoRenew[:-1])
      except ValueError:
        parser.error('Autorenewal must be expressed as integer followed immediately by either m for month or y for year')
    elif args.autoRenew[-1] == 'm':
      try:
        autoRenew = "%d month periods"%int(args.autoRenew[:-1])
      except ValueError:
        parser.error('Autorenewal must be expressed as integer followed immediately by either m for month or y for year')
    else:
      parser.error('Only m (month) and y (year) are supported as autorenewal periods')
  else:
    parser.error('Autorenewal must be expressed as integer followed immediately by either m for month or y for year')
else:
  autoRenew = None
if args.notice:
  try:
    notice = int(args.notice)
  except ValueError:
    parser.error('Notice must be an integer, not %s. By default, notice is not set'%args.notice)
else: notice=None
if args.relatedTo:
  relatedTo = args.relatedTo
  # do an os.listdir in the processed directory to check if files start with
  # the relatedTo uniqId - otherwise don't allow it
else: relatedTo = None
if args.tags:
  tags = args.tags
else:
  tags = None
if args.parties:
  parties = args.parties
else:
  parties = None
if args.signatories:
  signatories = args.signatories
else:
  signatories = None
if not args.status:
  try:
    if datetime.now() >= effDate and termDate == 'until terminated':
      status = 'active'
    elif datetime.now() >= effDate and datetime.now() < termDate:
      status = 'active'
    elif datetime.now() > termDate:
      status = 'expired'
    else:
      status = 'unknown'
  except:
    status = 'unknown'
else:
  try:
    s = args.status.lower()
  except:
    parser.error('status must be a string - choose between active, expired, terminated, amended or unknown')
  else:
    if s not in ['active','expired','terminated','amended','unknown']:
      parser.error('status must be either active, expired, terminated, amended or unknown - not %s'%s)
    else:
      status = s  
if args.desc:
  desc = args.desc
else:
  desc = None

cp = comemaProcessor(infile,effDate=effDate,termDate=termDate,autoRenew=autoRenew,
    notice=notice,tags=tags,parties=parties,relatedTo=relatedTo,
    signatories=signatories,status=status,desc=desc)
cp.tryAgain()
